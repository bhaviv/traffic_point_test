<?php

namespace App\Http\Controllers;

use Flash;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;

class PostCommentController extends Controller
{
    /**
     * Insert new comment to the db after validation is passed.
     *
     * @param Request $request
     * @param int $postId - post number id for this comment.
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, $postId)
    {
        $this->validate($request, ['content' => 'required|min:10']);
        $post = Post::find($postId);
        if (!$post) {
            Flash::error('Error', 'Fail to add your comment');

            return redirect(route('post.index'));
        }

        $post->addComment(new Comment($request->all()));
        Flash::success('Good', 'Your comment has been saved');

        return redirect(route('post.show', $postId));
    }
}
