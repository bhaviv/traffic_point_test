<?php

namespace App\Http\Controllers;

use DB;
use Flash;
use App\Models\Post;
use App\Models\Comment;
use App\Http\Requests\PostRequest;

class PostController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Show a list of posts
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = DB::table('posts')->paginate(10);

        return view('post.index', compact('posts'));
    }

    /**
     * Show one post and it's related comments
     *
     * @param int $id - post id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $post = Post::find($id);
        $comment = new Comment();

        return view('post.show', compact('post', 'comment'));
    }

    /**
     * Show form for creating a new post
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $post = new Post();

        return view('post.create', compact('post'));
    }

    /**
     * Save new post after validation test in case of error return with displaying errors.
     *
     * @param PostRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PostRequest $request)
    {
        $post = new Post($request->all());
        $post->save();

        Flash::success('Good', 'Your post has been saved');

        return redirect(route('post.show', $post->id));
    }

    /**
     * Show edit form for editing a post.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::find($id);
        if (!$post) {
            Flash::success('Error', 'Post not found');

            return redirect(route('post.index'));
        }

        return view('post.edit', compact('post'));
    }

    /**
     * Update an existing post.
     *
     * @param PostRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PostRequest $request, $id)
    {
        $post = Post::find($id);
        if (!$post) {
            Flash::success('Error', 'Post not found');

            return redirect(route('post.index'));
        }
        $post->title = $request->title;
        $post->body = $request->body;
        $post->save();
        Flash::success('Success', 'Your request has been updated');

        return redirect(route('post.show', $id));
    }

    /**
     * Delete a post from the db.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Post::find($id)->delete();
        Flash::info('Deleted', 'Your post has been Deleted');

        return redirect(route('post.index'));
    }
}
