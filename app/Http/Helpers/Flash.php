<?php

namespace App\Http\Helpers;

class Flash
{
    /**
     * Create flash  session.
     *
     * @param string $title - Flash message title.
     * @param string $text - Flash messagae text body.
     * @param string $type - Flash message type one of "warning", "error", "success" and "info"
     * @param bool $showConfirmButton - Whether to show ot not to show confirmation button default false.
     * @param mixed $timer - false for not using timer,  otherwise a string with millisecond number indicating time for the message to show on the screen default 1700 millisecond.
     */
    public static function create(
        $title,
        $text,
        $type,
        $showConfirmButton = false,
        $timer = '1700'
    ) {
        $flashMessage = [
            'title' => "\"$title\"",
            'text' => "\"$text\"",
            'type' => "\"$type\"",
        ];
        $flashMessage['showConfirmButton'] = $showConfirmButton ? 'true' : 'false';
        $timer !== false ? $flashMessage['timer'] = $timer : null;

        session()->flash('flash_message', $flashMessage);
    }

    /**
     * Create a warning flash message.
     *
     * @param string $title
     * @param string $text
     */
    public static function warning($title, $text)
    {
        self::create($title, $text, 'warning');
    }

    /**
     * Create an error flash message.
     *
     * @param string $title
     * @param string $text
     */
    public static function error($title, $text)
    {
        self::create($title, $text, 'error');
    }

    /**
     * Create a warning flash message.
     *
     * @param string $title
     * @param string $text
     */
    public static function success($title, $text)
    {
        self::create($title, $text, 'success');
    }

    /**
     * Create an info flash message.
     *
     * @param string $title
     * @param string $text
     */
    public static function info($title, $text)
    {
        self::create($title, $text, 'info');
    }

    public static function overlayInfo($title, $text)
    {
        self::create($title, $text, 'info', true, false);
    }

    public static function overlayError($title, $text)
    {
        self::create($title, $text, 'error', true, false);
    }
}
