<div class="row">

    @include('components._error')
    <h2 class="section-title no-margin-top">write you own comment</h2>
    {!! Form::model( $comment, [ 'route' => ['post.comment.store', $post->id  ]]) !!}
      <div class="form-group">
          {!! Form::textarea('content', null,
              array('required',
                    'class'=>'form-control',
                    'placeholder'=>'add your comment')) !!}
      </div>

      <div class="form-group">
          <button type="submit" class="btn btn-md btn-primary">Save</button>
      </div>
    {!! Form::close() !!}

</div>
