<div class="row">
     @foreach ($post->comments as $comment)
        <div class="panel panel-info">
            <div class="panel-body">
                {{$comment->content}}
            </div>
        </div>
     @endforeach
</div>
