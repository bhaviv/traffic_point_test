@extends('layouts.main')

@section('content')
    <div class="container page-container">
        @include('components._error')
        <div class="row">
            <h2 class="section-title no-margin-top">Create a new post</h2>
            {!! Form::model( $post, [ 'route' => ['post.store'  ]]) !!}
            @include('post._form')
            {!! Form::close() !!}

        </div>
    </div>
@endsection

