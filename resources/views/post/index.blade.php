@extends('layouts.main')

@section('content')
    <div class="container page-container">
        <div class="row">
            <h2 class="section-title no-margin-top">Your Posts</h2>
             @foreach ($posts as $post)
                <div class="panel panel-info">
                    <div class="panel-heading ">
                        <h3 class="panel-title">
                            {{$post->title}}
                        </h3>
                    </div>
                    <div class="panel-body">
                        {{$post->body}}
                    </div>
                    <div class="panel-footer">
                        <a href="{{route('post.show', ['id' => $post->id])}}" class="btn btn-xs btn-primary">Preview</a>
                        <a href="{{route('post.edit', ['id' => $post->id])}}" class="btn btn-xs btn-primary">Edit</a>
                    </div>
                </div>
            @endforeach
            {{ $posts->links() }}

        </div>
    </div>
@endsection



