@extends('layouts.main')

@section('content')
    <style>
        .inline-form, .inline-form form { display: inline; }
    </style>
    <div class="container page-container">

        <div class="row">
            <h2 class="section-title no-margin-top">Your Post</h2>
            <div class="panel panel-info">
                <div class="panel-heading ">
                    <h3 class="panel-title">
                        {{$post->title}}
                    </h3>
                </div>
                <div class="panel-body">
                    {!! nl2br($post->body) !!}
                </div>
                <div class="panel-footer">
                    <a href="{{route('post.edit', ['id' => $post->id])}}" class="btn btn-xs btn-primary">Edit</a>

                    <div class="inline-form">
                    {{ Form::open(['route' => ['post.destroy', $post->id], 'method' => 'delete']) }}
                       <button type="submit" class="btn btn-xs btn-danger" >Delete</button>
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
        <h3>Comments</h3>
        @include('comment.index');
        @include('comment.create');
    </div>
@endsection

