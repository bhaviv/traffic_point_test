@extends('layouts.main')

@section('content')
    <div class="container page-container">
        @include('components._error')
        <div class="row">
            <h2 class="section-title no-margin-top">Edit post</h2>
            {!! Form::model( $post, [ 'route' => ['post.update', $post->id  ], 'method' => 'PUT']) !!}


            @include('post._form')
            {!! Form::close() !!}

        </div>
    </div>
@endsection

