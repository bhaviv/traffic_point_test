<div class="form-group">
    {!! Form::label('Post title') !!}
    {!! Form::text('title', null,
        array('required',
              'class'=>'form-control',
              'placeholder'=>'Your title')) !!}
</div>

<div class="form-group">
    {!! Form::label('Body') !!}
    {!! Form::textarea('body', null,
        array('required',
              'class'=>'form-control',
              'placeholder'=>'start write here')) !!}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-md btn-primary">Save</button>
    <button type="button" onclick="window.history.back(); return false;" >Cancel</button>
</div>

