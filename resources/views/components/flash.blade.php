@if (Session::has('flash_message'))
    <script type="text/javascript">
        swal({
            @foreach( Session::get('flash_message') as $key => $value )
              {!! $key .': '. $value  . ($loop->last ? '': ',') !!}
            @endforeach
        });
    </script>
@endif

