<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Traffic Point</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="{{route('post.index')}}">Posts </a></li>
        <li><a href="{{route('post.create')}}">New post</a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="/traffic_point_js_tests/event_manager.html" target="_blank" >Event Manager</a></li>
        <li><a href="/traffic_point_js_tests/event_manager_tests.html" target="_blank" >Event Manager tests</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
