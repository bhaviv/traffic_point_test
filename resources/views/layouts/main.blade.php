<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Traffic Point test</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
       <link rel="stylesheet" type="text/css" href="/css/app.css">
       <link rel="stylesheet" type="text/css" href="/css/lib/sweetalert.css">
    </head>
    <body>
        @include('layouts.nav-bar')

        <div class="container">
            @yield('content');
        </div>


       <script src="/js/lib/sweetalert.min.js"></script>
       @include('components.flash')
       @yield('javascript')
    </body>
</html>
