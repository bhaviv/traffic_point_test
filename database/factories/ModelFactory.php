<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Post::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->sentence(),
        'body' => $faker->paragraphs(3,true)
    ];
});


$factory->define(App\Models\Comment::class, function (Faker\Generator $faker) {

    return [
        'content' => $faker->paragraphs(2,true)
    ];
});
