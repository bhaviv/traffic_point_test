<?php

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\Comment;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(Post::class,100)->create()->each(function($post) {
          $post->comments()->save(factory(Comment::class)->make());
          $post->comments()->save(factory(Comment::class)->make());
          $post->comments()->save(factory(Comment::class)->make());
      });;
    }
}
