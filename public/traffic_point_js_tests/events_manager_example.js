/*
 CHALLENGE 1 A : Please write the source code of “EventsManager” Object
 */

//@@@@@@@@@@Your Code for EventsManager
var EventsManager = function() {
  var events = {};

  var on = function(eventName, callback, context) {
    if( events[eventName]) {
      events[eventName].push([callback, context])
    } else {
      events[eventName] = [[callback, context]];
    }
  };

  var off = function(eventName, callback) {
    if( events[eventName]) {
      if (! callback) {
        delete events[eventName];
      } else {
        events[eventName].forEach(function(event, index, events) {
          if( event[0] === callback ) {
            events.splice(index, 1);
          }
        });
      }
    }
  };

  trigger = function(eventName) {
    if(events[eventName]) {
      var args =  Array.prototype.slice.call(arguments, 1);
      events[eventName].forEach(function(eventData) {
        var context = eventData[1] ? eventData[1] : this;
        eventData[0].apply(context, args);
      });
    }
  };

  return {
    on: on,
    off: off,
    trigger: trigger
  }
};

var MyEventsManager = EventsManager();
/*
 CHALLENGE 1 B : Please write the source code of “Person”
 */
var Person = function(name, eventManager) {
  this.name = name;
  this.foods = [];
  this.eventManager = eventManager;
};

//###############################3
//#################################

Person.prototype.waitToEat = function() {
  this.eventManager.on('breakfast:ready', this.eat, this);
};

Person.prototype.eat = function(foods, s1, s2) {
  console.log("i'm", this.name, "and i'm eating", foods.join(","));
  this.foods.length = 0;
  this.foods = foods.concat(s1).concat(s2);
  this.eventManager.trigger('eat:done', this);
};

Person.prototype.finishEat = function(time) {
  console.log("i'm", this.name, "and i finished eating at", time);
  this.eventManager.off("breakfast:ready", this.finishEat);
};


// logFood method has a task ahead
Person.prototype.logFood = function() {
  this.foods.forEach(function(item) {
    console.log("I'm " + this.name + " and I ate " + item);
  }, this);
};

Person.prototype.logFood_v2 = function() {
  var self = this;
  this.foods.forEach(function(item){
    console.log("I'm " + self.name + " and I ate " + item);
  });
};
/*
 NOTICE: After you add your code of EventsManager you should run
 all the code and test your success with the code below.
 Meaning, the code below should work without any errors
 */
// start the app
MyEventsManager.on('eat:done', function(person){
  console.log(person.name, "finished eating");
});

MyEventsManager.on('breakfast:ready', function(menu, m2, m3){
  console.log("breakfast is ready with:", menu, m2, m3);
});

var john = new Person('john', MyEventsManager);
john.waitToEat();
MyEventsManager.on('eat:done', function(person){
  person.finishEat(new Date());
});

var breakfast = ["scrambled eggs", "tomatoes", "bread", "butter"];
var more = ["pizza", "spaghetti"];
var desserts = ["cake", "ice cream"];
MyEventsManager.trigger('breakfast:ready', breakfast, more, desserts);


/*
 CHALLENGE 2: Please FIX the source code of “logFood” according to the
 instructions:
 this “logFood” method throws an error.
 "this.name" doesn't print the Person's name
 Please suggest 2 di0erent solutions (by adding the relevant /x code)
 so "this.name" will print the relevant name
 */

console.log("** using logFood v1 **");
john.logFood();
console.log("** using logFood v2 **");
john.logFood_v2();



if(typeof window !== "undefined" ) {
  Person.prototype.appendHtmlMsg = function(msg) {
    var msgElement = document.createElement("LI");
    msgElement.appendChild(document.createTextNode(msg));
    this.htmlLog.appendChild(msgElement);
  };

  Person.prototype.htmlLog = function() {
    this.htmlLog = document.getElementById('html-log');
    if(! this.htmlLog) return;
    this.foods.forEach(function(item) {
      var msg = "I'm " + this.name + " and I ate " + item;
      this.appendHtmlMsg(msg);
    }, this);
  };

  window.onload = function() {
    john.htmlLog();
  }
}


