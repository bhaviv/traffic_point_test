(function() {
  mocha.setup('bdd');
  var expect = chai.expect;

  describe("EventsManager", function() {
    describe("public functions", function() {
      var em = EventsManager();
      it("should have on function", function() {
        expect(typeof em.on).to.equal("function");
      });

      it("should have off function", function() {
        expect(typeof em.off).to.equal("function");
      });

      it("should have trigger function", function() {
        expect(typeof em.trigger).to.equal("function");
      });

    });

    describe("Events", function() {
      it("should trigger event", function(done){
        var em = EventsManager();
        em.on('test:event', function() {
          done();
        });
        em.trigger('test:event');
      });

      it("should trigger event with multiple params", function(done){
        var em = EventsManager();
        em.on('test:event', function(p1 , p2) {
          expect(p1).to.equal('p1');
          expect(p2).to.equal('p2');
          done();
        });
        em.trigger('test:event', 'p1','p2');
      });

      it("should cancel event for all callbacks", function(done){
        var em = EventsManager();
        var shouldNotBeRun1 = function() {
          done( new Error("error"));
        };

        var shouldNotBeRun2 = function() {
          done( new Error("error"));
        };

        em.on('test:event', shouldNotBeRun1);
        em.on('test:event', shouldNotBeRun2);

        em.off('test:event');
        em.trigger('test:event');
        setTimeout(function(){ done();},200);
      });

      it("should cancel event for specific callback", function(done){
        var em = EventsManager();
        var shouldNotBeRun = function() {
          done( new Error("error"));
        };

        var shouldBeRun = function() {
          done();
        };

        em.on('test:event', shouldNotBeRun);
        em.on('test:event', shouldBeRun);

        em.off('test:event', shouldNotBeRun);
        em.trigger('test:event');
      });
    });

  });


  mocha.run();
})();
